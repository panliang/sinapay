<?php

namespace Ibeibeili\SinaPay;

class BindingBankCard extends AbstractPaymentApi
{
    /**
     * 发起请求
     *
     * @param  array $params
     * @return mixed
     */
    public function send($params = [])
    {

        $params = $this->getParams($params);
        
        $data = $this->sinapay->createCurlData($params);

        $result = $this->sinapay->curlPost($this->config['mgs_url'], $data);

        $result = json_decode($result, true);

        return $result;
    }

    /**
     * 获取提交参数
     *
     * @param  array $params
     * @return array
     */
    protected function getParams($params = [])
    {
        $params = array_merge($this->defaultParams(), $params);

        $params['bank_account_no'] = $this->sinapay->defaultRsaEncrypt(@$params['bank_account_no']);

        $params['phone_no'] = $this->sinapay->defaultRsaEncrypt(@$params['phone_no']);

        ksort($params);

        $params['sign'] = $this->sinapay->getSignMsg($params, @$params['sign_type']);

        return $params;
    }
}