<?php

namespace Ibeibeili\SinaPay;

use Ibeibeili\SinaPay\Log\Logger;
use Monolog\Logger as MonologLogger;
use Log;

class SinaPay
{
    /**
     * 配置信息
     *
     * @return array
     */
    protected $config;

    protected $uri='http://test.ibeibeili.com';

    public function __construct($config)
    {
        $this->config = $config;
    }

    /**
     * 通过公钥进行rsa加密
     *
     * @param type $name
     *          Descriptiondata
     *          $data 需要进行rsa公钥加密的数据 必传
     *          $pu_key 加密所使用的公钥 必传
     * @return 加密好的密文
     */
    public function rsaEncrypt($data, $public_key) {
        $encrypted = "";
        $cert = file_get_contents ( $public_key );
        $pu_key = openssl_pkey_get_public ( $cert ); // 这个函数可用来判断公钥是否是可用的
        openssl_public_encrypt ( $data, $encrypted, $pu_key ); // 公钥加密
        $encrypted = base64_encode ( $encrypted ); // 进行编码
        return $encrypted;
    }

    /**
     * 默认使用新浪公钥加密
     *
     * @param  mixed $data
     * @return mixed
     */
    public function defaultRsaEncrypt($data) {
        return $this->rsaEncrypt($data, $this->config['rsa_public_key']);
    }

    /**
     * 默认使用新浪公钥批量加密
     *
     * @param  array $params
     * @return mixed
     */
    public function mutilDefaultRsaEncrypt($params = [])
    {
        $newParams = [];

        foreach ($params as $key => $value) {
            $newParams[$key] = $this->defaultRsaEncrypt($value);
        }

        return $newParams;
    }

    /**
     * getSignMsg 计算签名
     *
     * @param array $params 计算签名数据
     * @param string $signType 签名类型
     * @return string $signMsg 返回密文
     */
    public function getSignMsg($params = [], $signType = 'MD5') {
        $paramsString = "";
        $signMsg = "";
        
        foreach ( $params as $key => $val ) {
            if ($key != "sign" && $key != "sign_type" && $key != "sign_version" && isset ( $val ) && @$val != "") {
                $paramsString .= $key . "=" . $val . "&";
            }
        }

        $paramsString = substr ( $paramsString, 0, - 1 );

        switch (@$signType) {
            case 'RSA' :
                $this->log("RSA参与签名运算数据：\t{$paramsString}");
                $priv_key = file_get_contents ( $this->config['rsa_sign_private_key'] );
                $pkeyid = openssl_pkey_get_private ( $priv_key );
                openssl_sign ( $paramsString, $signMsg, $pkeyid, OPENSSL_ALGO_SHA1 );
                //从内存中释放私钥
                openssl_free_key ( $pkeyid );
                $signMsg = base64_encode ( $signMsg );
                $this->log("RSA计算得出签名值：\t{$signMsg}");
                break;
            case 'MD5' :
            default :
                $paramsString = $paramsString . @$this->config['md5_key'];
                $this->log("MD5参与签名运算数据：\t{$paramsString}");
                $signMsg = strtolower ( md5 ( $paramsString ) );
                $this->log("MD5计算得出签名值：\t{$signMsg}");
                break;
        }
        return $signMsg;
    }

    /**
     * checkSignMsg 回调签名验证
     *
     * @param  array $params         
     * @param  string $signType         
     * @return boolean
     */
    public function checkSignMsg($params = [], $signType = 'MD5')
    {
        $paramsString = "";
        $signMsg = "";
        $return = false;
        foreach ( $params as $key => $val ) {
            if ($key != "sign" && $key != "sign_type" && $key != "sign_version" && ! is_null ( $val ) && @$val != "") {
                $paramsString .= $key . "=" . $val . "&";
            }
        }

        $paramsString = substr ( $paramsString, 0, - 1 );
        switch (@$signType) {
            case 'RSA' :
                $cert = file_get_contents ( $this->config['rsa_sign_public_key'] );
                $pubkeyid = openssl_pkey_get_public ( $cert );
                $ok = openssl_verify ( $paramsString, base64_decode ( $params ['sign'] ), $cert, OPENSSL_ALGO_SHA1 );
                $return = $ok == 1 ? true : false;
                //从内存中释放公钥
                openssl_free_key ( $pubkeyid );
                break;
            case 'MD5' :
            default :
                $paramsString = $paramsString . @$this->config['md5_key'];
                $signMsg = strtolower ( md5 ( $paramsString ) );
                $return = (@$signMsg == @strtolower ( $params ['sign'] )) ? true : false;
                break;
        }

        return $return;
    }

    /**
     * 生成 curl 模拟提交数据
     *
     * @param  array $params
     * @return string url 格式字符串
     */
    public function createCurlData($params = [])
    {
        $paramsString = '';

        foreach ($params as $key => $val) {
            if (isset ( $val ) && ! is_null ( $val ) && @$val != "") {
                $paramsString .=  $key . "=" . urlencode ( urlencode ( trim ( $val ) ) ) . "&";
            }
        }

        $paramsString = substr ( $paramsString, 0, - 1 );

        return $paramsString;
    }

    /**
     * curl 模拟提交
     *
     * @param  string
     * @param  string
     * @return string
     */
    public function curlPost($url, $data)
    {
        $this->log("请求sina网关地址\t{$url}\n请求sina网关数据\t{$data}");

        $curl = curl_init ();

        curl_setopt ( $curl, CURLOPT_URL, $url );
        curl_setopt ( $curl, CURLOPT_POST, 1 );
        curl_setopt ( $curl, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt ( $curl, CURLOPT_POSTFIELDS, $data );
        curl_setopt ( $curl, CURLOPT_SSL_VERIFYHOST, 0 );
        curl_setopt ( $curl, CURLOPT_SSL_VERIFYPEER, 0 );

        $results = urldecode(curl_exec ( $curl ));

        $returnCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        $this->log("请求sina网关返回结果[http_response_code => {$returnCode}]\t{$results}");

        curl_close ( $curl );
        
        return $results;
    }

    /**
     * sftp上传企业资质
     * sftp upload
     * @param $file 上传文件路径
     * @return false 失败   true 成功
     */
    public function sftpUpload($file, $filename) {
        $strServer = $this->config['sftp_address'];
        $strServerPort = $this->config['sftp_port'];
        $strServerUsername = $this->config['sftp_username'];
        $strServerprivatekey = $this->config['sftp_privatekey'];
        $strServerpublickey = $this->config['sftp_publickey'];
        $resConnection = ssh2_connect ( $strServer, $strServerPort );
        if (ssh2_auth_pubkey_file ( $resConnection, $strServerUsername, $strServerpublickey, $strServerprivatekey )) 
        {
            $resSFTP = ssh2_sftp ( $resConnection );
            file_put_contents ( "ssh2.sftp://{$resSFTP}/upload/".$filename, $file);
            if (! copy ( $file, "ssh2.sftp://{$resSFTP}/upload/$filename" )) {
                return false;
            }
            return true;
        }
        return false;
    }

    /**  
     * sftp下载文件
     * sftp upload
     * @param $file 保存zip 下载文件路径
     * @param $filename 下载文件名称
     * @return FAIL 失败   SUCCESS 成功
     */
    public function sftpDownload($file,$filename) {
        $start_time=microtime(true);
        $strServer = $this->config['sftp_address'];
        $strServerPort = $this->config['sftp_port'];
        $strServerUsername = $this->config['sftp_username'];
        $strServerprivatekey = $this->config['sftp_privatekey'];
        $strServerpublickey = $this->config['sftp_publickey'];
        $resConnection = ssh2_connect ($strServer, $strServerPort );
        if (ssh2_auth_pubkey_file ( $resConnection, $strServerUsername, $strServerpublickey, $strServerprivatekey )) {
            $resSFTP = ssh2_sftp ( $resConnection );
            $opts = array(
                'http'=>array(
                    'method'=>"GET",
                    'timeout'=>60,
                )
            );
            $context = stream_context_create($opts);
            $strData = file_get_contents("ssh2.sftp://{$resSFTP}/upload/busiexport/$filename", false, $context);
            if (! file_put_contents($file.$filename, $strData)) {
                $end_time=microtime(true);//获取程序执行结束的时间
                $total=$end_time-$start_time; //计算差值
                 $this->log($filename."下载失败，耗时".$total."秒");
                return false;
            }else{
                $end_time=microtime(true);//获取程序执行结束的时间
                $total=$end_time-$start_time; //计算差值
                $this->log($filename."下载成功，耗时".$total."秒");
            }
        }
        return true;
    }

    /**
     * @param $path 需要创建的文件夹目录
     * @return bool true 创建成功 false 创建失败
     */
    public function mkFolder($path)
    {
        $this->log("开始创建文件夹");
        if (!file_exists($path))
        {
            mkdir($path, 0777);
            $this->log("文件夹创建成功".$path);
            return true;
        }
        $this->log("文件夹创建失败".$path);
        return false;
    }

    /**
     * 记录调试日志消息 
     *
     * @param  string $message
     * @param  array $context
     * @return void
     */
    protected function log($message, array $context = [])
    {
        if (@$this->config['debug_status']) {
            $logger = new Logger(new MonologLogger('sinapay'));

            $logger->useDailyFiles($this->getLoggerPath(), 30);

            $logger->debug($message, $context);
        }
    }


    /**
     * 日志输出地址
     *
     * @return string
     */
    protected function getLoggerPath()
    {
        if (@$this->config['logger_storage']) {
            return $this->config['logger_storage'] . '/sinapay.log';
        }

        return __DIR__.'/../logs/sinapay.log';
    }
}