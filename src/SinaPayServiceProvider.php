<?php

namespace Ibeibeili\SinaPay;

use Illuminate\Support\ServiceProvider;

class SinaPayServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->app->singleton('payment.sinapay', function($app){
            return new SinaPay($this->getConfig());
        });

        // 创建激活会员
        $this->app->singleton('payment.sinapay.create_activate_member', function($app){
            return new CreateActivateMember($this->getSinaPay(), $this->getConfig());
        });

        // 银行卡绑定
        $this->app->singleton('payment.sinapay.binding_bank_card', function($app){
            return new BindingBankCard($this->getSinaPay(), $this->getConfig());
        });

        // 机构设置经办人信息
        $this->app->singleton('payment.sinapay.smt_fund_agent_buy', function($app){
            return new SmtFundAgentBuy($this->getSinaPay(), $this->getConfig());
        });

        //银行卡推进
        $this->app->singleton('payment.sinapay.binding_bank_card_advance', function($app){
            return new BindingBankCardAdvance($this->getSinaPay(), $this->getConfig());
        });

        //解绑银行卡
        $this->app->singleton('payment.sinapay.unbinding_bank_card', function($app){
            return new UnbindingBankCard($this->getSinaPay(), $this->getConfig());
        });

        //解绑银行卡推进
        $this->app->singleton('payment.sinapay.unbinding_bank_card_advance', function($app){
            return new UnbindingBankCardAdvance($this->getSinaPay(), $this->getConfig());
        });

        //查询银行卡
        $this->app->singleton('payment.sinapay.query_bank_card', function($app){
            return new QueryBankCard($this->getSinaPay(), $this->getConfig());
        });

        //转账接口
        $this->app->singleton('payment.sinapay.create_hosting_transfer', function($app){
            return new CreateHostingTransfer($this->getSinaPay(), $this->getConfig());
        });

        //设置支付密码
        $this->app->singleton('payment.sinapay.set_pay_password', function($app){
            return new SetPayPassword($this->getSinaPay(), $this->getConfig());
        });

        //找回支付密码
        $this->app->singleton('payment.sinapay.find_pay_password', function($app){
            return new FindPayPassword($this->getSinaPay(), $this->getConfig());
        });

        //找回支付密码
        $this->app->singleton('payment.sinapay.modify_verify_mobile', function($app){
            return new ModifyVerifyMobile($this->getSinaPay(), $this->getConfig());
        });

         //设置支付密码
        $this->app->singleton('payment.sinapay.modify_pay_password', function($app){
            return new ModifyPayPassword($this->getSinaPay(), $this->getConfig());
        });

        // 设置用户实名认证
        $this->app->singleton('payment.sinapay.set_real_name', function($app){
            return new SetRealName($this->getSinaPay(), $this->getConfig());
        });

        // 请求审核企业会员
        $this->app->singleton('payment.sinapay.audit_member_infos', function($app){
            return new AuditMemberInfos($this->getSinaPay(), $this->getConfig());
        });

        //绑定认证信息
        $this->app->singleton('payment.sinapay.binding_verify', function($app){
            return new BindingVerify($this->getSinaPay(), $this->getConfig());
        });

        //绑定认证信息
        $this->app->singleton('payment.sinapay.unbinding_verify', function($app){
            return new UnbindingVerify($this->getSinaPay(), $this->getConfig());
        });

        //查询认证信息
        $this->app->singleton('payment.sinapay.query_verify', function($app){
            return new QueryVerify($this->getSinaPay(), $this->getConfig());
        });

        //查询余额
        $this->app->singleton('payment.sinapay.query_balance', function($app){
            return new QueryBalance($this->getSinaPay(), $this->getConfig());
        });

         //查询余额
        $this->app->singleton('payment.sinapay.query_member_infos', function($app){
            return new QueryBalance($this->getSinaPay(), $this->getConfig());
        });

        //查询是否设置支付密码
        $this->app->singleton('payment.sinapay.query_is_set_pay_password', function($app){
            return new QueryIsSetPayPassword($this->getSinaPay(), $this->getConfig());
        });

        //托管充值
        $this->app->singleton('payment.sinapay.create_hosting_deposit', function($app){
            return new CreateHostingDeposit($this->getSinaPay(), $this->getConfig());
        });

        //托管提现
        $this->app->singleton('payment.sinapay.create_hosting_withdraw', function($app){
            return new CreateHostingWithdraw($this->getSinaPay(), $this->getConfig());
        });

        //托管充值查询
        $this->app->singleton('payment.sinapay.query_hosting_deposit', function($app){
            return new QueryHostingDeposit($this->getSinaPay(), $this->getConfig());
        });

        //托管提现查询
        $this->app->singleton('payment.sinapay.query_hosting_withdraw', function($app){
            return new QueryHostingWithdraw($this->getSinaPay(), $this->getConfig());
        });

        //托管交易支付
        $this->app->singleton('payment.sinapay.pay_hosting_trade', function($app){
            return new PayHostingTrade($this->getSinaPay(), $this->getConfig());
        });

        //创建托管代收交易
        $this->app->singleton('payment.sinapay.create_hosting_collect_trade', function($app){
            return new CreateHostingCollectTrade($this->getSinaPay(), $this->getConfig());
        });

        //创建托管代付交易
        $this->app->singleton('payment.sinapay.create_single_hosting_pay_trade', function($app){
            return new CreateSingleHostingPayTrade($this->getSinaPay(), $this->getConfig());
        });

        //创建批量托管代付交易
        $this->app->singleton('payment.sinapay.create_batch_hosting_pay_trade', function($app){
            return new CreateBatchHostingPayTrade($this->getSinaPay(), $this->getConfig());
        });
        //代收撤销
        $this->app->singleton('payment.sinapay.cancel_pre_auth_trade', function($app){
            return new CancelPreAuthTrade($this->getSinaPay(), $this->getConfig());
        });
        //托管交易查询
        $this->app->singleton('payment.sinapay.query_hosting_trade', function($app){
            return new QueryHostingTrade($this->getSinaPay(), $this->getConfig());
        });

        $this->app->singleton('payment.finish_pre_auth_trade', function($app){
            return new FinishPreAuthTrade($this->getSinaPay(), $this->getConfig());
        });
        //创建单笔代付到提现卡交易
        $this->app->singleton('payment.sinapay.create_single_hosting_pay_to_card_trade', function($app){
            return new CreateSingleHostingPayToCardTrade($this->getSinaPay(), $this->getConfig());
        });
        //创建批量代付到提现卡交易
        $this->app->singleton('payment.sinapay.create_batch_hosting_pay_to_card_trade', function($app){
            return new CreateBatchHostingPayToCardTrade($this->getSinaPay(), $this->getConfig());
        });
        //标的录入
        $this->app->singleton('payment.sinapay.create_bid_info', function($app){
            return new CreateBidInfo($this->getSinaPay(), $this->getConfig());
        });
        //冻结金额
        $this->app->singleton('payment.sinapay.balance_freeze', function($app){
            return new BalanceFreeze($this->getSinaPay(), $this->getConfig());
        });
        //解冻金额
        $this->app->singleton('payment.sinapay.balance_unfreeze', function($app){
            return new BalanceUnfreeze($this->getSinaPay(), $this->getConfig());
        });
        //查询冻结解冻结果
        $this->app->singleton('payment.sinapay.query_ctrl_result', function($app){
            return new QueryCtrlResult($this->getSinaPay(), $this->getConfig());
        });
        //个人中心
        $this->app->singleton('payment.sinapay.show_member_infos_sina', function($app){
            return new ShowMemberInfosSina($this->getSinaPay(), $this->getConfig());
        });

        // 标的查询
        $this->app->singleton('payment.sinapay.query_bid_info',function($app){
            return new QueryBidInfo($this->getSinaPay(), $this->getConfig());
        });
    }

    public function register()
    {
        $configPath = __DIR__ . '/../config/sinapay.php';

        $this->mergeConfigFrom($configPath, 'sinapay');

        $this->publishes([
            $configPath => config_path('sinapay.php'),
            __DIR__ . '/../key' => storage_path('key'),
        ]);
    }

    /**
     * 获取新浪支付实例
     *
     * @return \Ibeibeili\SinaPay\SinaPay
     */
    protected function getSinaPay()
    {
        return $this->app['payment.sinapay'];
    }

    /**
     * 获取配置信息
     *
     * @return array
     */
    protected function getConfig()
    {
        return $this->app['config']['sinapay'];;
    }
}