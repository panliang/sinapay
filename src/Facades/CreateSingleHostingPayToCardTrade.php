<?php

namespace Ibeibeili\SinaPay\Facades;

use Illuminate\Support\Facades\Facade;

class CreateSingleHostingPayToCardTrade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'payment.sinapay.create_single_hosting_pay_to_card_trade';
    }
}