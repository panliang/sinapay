<?php

namespace Ibeibeili\SinaPay\Facades;

use Illuminate\Support\Facades\Facade;

class CreateBatchHostingPayToCardTrade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'payment.sinapay.create_batch_hosting_pay_to_card_trade';
    }
}