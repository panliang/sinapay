<?php

namespace Ibeibeili\SinaPay\Facades;

use Illuminate\Support\Facades\Facade;

class SetRealName extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'payment.sinapay.set_real_name';
    }
}