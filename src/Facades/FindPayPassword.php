<?php

namespace Ibeibeili\SinaPay\Facades;

use Illuminate\Support\Facades\Facade;

class FindPayPassword extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'payment.sinapay.find_pay_password';
    }
}