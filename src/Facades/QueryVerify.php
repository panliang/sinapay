<?php

namespace Ibeibeili\SinaPay\Facades;

use Illuminate\Support\Facades\Facade;

class QueryVerify extends Facade
{
  protected static function getFacadeAccessor()
  {
    return 'payment.sinapay.query_verify';
  }
}