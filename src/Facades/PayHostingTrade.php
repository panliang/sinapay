<?php

namespace Ibeibeili\SinaPay\Facades;

use Illuminate\Support\Facades\Facade;

class PayHostingTrade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'payment.sinapay.pay_hosting_trade';
    }
}