<?php

namespace Ibeibeili\SinaPay\Facades;

use Illuminate\Support\Facades\Facade;

class QueryMemberInfos extends Facade
{
  protected static function getFacadeAccessor()
  {
    return 'payment.sinapay.query_member_infos';
  }
}