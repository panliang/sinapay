<?php

namespace Ibeibeili\SinaPay\Facades;

use Illuminate\Support\Facades\Facade;

class FinishPreAuthTrade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'payment.finish_pre_auth_trade';
    }
}