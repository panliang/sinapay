<?php

namespace Ibeibeili\SinaPay\Facades;

use Illuminate\Support\Facades\Facade;

class UnbindingVerify extends Facade
{
  protected static function getFacadeAccessor()
  {
    return 'payment.sinapay.unbinding_verify';
  }
}