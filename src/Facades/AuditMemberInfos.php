<?php

namespace Ibeibeili\SinaPay\Facades;

use Illuminate\Support\Facades\Facade;

class AuditMemberInfos extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'payment.sinapay.audit_member_infos';
    }
}