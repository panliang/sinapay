<?php

namespace Ibeibeili\SinaPay\Facades;

use Illuminate\Support\Facades\Facade;

class BindingBankCardAdvance extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'payment.sinapay.binding_bank_card_advance';
    }
}