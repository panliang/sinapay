<?php

namespace Ibeibeili\SinaPay\Facades;

use Illuminate\Support\Facades\Facade;

class ModifyVerifyMobile extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'payment.sinapay.modify_verify_mobile';
    }
}