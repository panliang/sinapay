<?php

namespace Ibeibeili\SinaPay\Facades;

use Illuminate\Support\Facades\Facade;

class CreateBidInfo extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'payment.sinapay.create_bid_info';
    }
}