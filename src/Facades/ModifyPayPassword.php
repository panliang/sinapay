<?php

namespace Ibeibeili\SinaPay\Facades;

use Illuminate\Support\Facades\Facade;

class ModifyPayPassword extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'payment.sinapay.modify_pay_password';
    }
}