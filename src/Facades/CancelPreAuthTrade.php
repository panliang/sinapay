<?php

namespace Ibeibeili\SinaPay\Facades;

use Illuminate\Support\Facades\Facade;

class CancelPreAuthTrade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'payment.sinapay.cancel_pre_auth_trade';
    }
}