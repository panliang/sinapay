<?php

namespace Ibeibeili\SinaPay\Facades;

use Illuminate\Support\Facades\Facade;

class SetPayPassword extends Facade
{
  protected static function getFacadeAccessor()
  {
    return 'payment.sinapay.set_pay_password';
  }
}