<?php

namespace Ibeibeili\SinaPay\Facades;

use Illuminate\Support\Facades\Facade;

class BalanceUnfreeze extends Facade
{
    protected static function getFacadeAccessor()
    {
      return 'payment.sinapay.balance_unfreeze';
    }
}