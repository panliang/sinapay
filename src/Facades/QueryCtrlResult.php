<?php

namespace Ibeibeili\SinaPay\Facades;

use Illuminate\Support\Facades\Facade;

class QueryCtrlResult extends Facade
{
    protected static function getFacadeAccessor()
    {
      return 'payment.sinapay.query_ctrl_result';
    }
}