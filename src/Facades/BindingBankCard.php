<?php

namespace Ibeibeili\SinaPay\Facades;

use Illuminate\Support\Facades\Facade;

class BindingBankCard extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'payment.sinapay.binding_bank_card';
    }
}