<?php

namespace Ibeibeili\SinaPay\Facades;

use Illuminate\Support\Facades\Facade;

class QueryHostingTrade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'payment.sinapay.query_hosting_trade';
    }
}