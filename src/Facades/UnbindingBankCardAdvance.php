<?php

namespace Ibeibeili\SinaPay\Facades;

use Illuminate\Support\Facades\Facade;

class UnbindingBankCardAdvance extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'payment.sinapay.unbinding_bank_card_advance';
    }
}