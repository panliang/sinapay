<?php

namespace Ibeibeili\SinaPay\Facades;

use Illuminate\Support\Facades\Facade;

class SmtFundAgentBuy extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'payment.sinapay.smt_fund_agent_buy';
    }
}