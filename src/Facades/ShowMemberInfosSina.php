<?php

namespace Ibeibeili\SinaPay\Facades;

use Illuminate\Support\Facades\Facade;

class ShowMemberInfosSina extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'payment.sinapay.show_member_infos_sina';
    }
}