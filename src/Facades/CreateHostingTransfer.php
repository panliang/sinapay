<?php

namespace Ibeibeili\SinaPay\Facades;

use Illuminate\Support\Facades\Facade;

class CreateHostingTransfer extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'payment.sinapay.create_hosting_transfer';
    }
}
