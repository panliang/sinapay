<?php

namespace Ibeibeili\SinaPay\Facades;

use Illuminate\Support\Facades\Facade;

class QueryBankCard extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'payment.sinapay.query_bank_card';
    }
}