<?php

namespace Ibeibeili\SinaPay\Facades;

use Illuminate\Support\Facades\Facade;

class QueryBidInfo extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'payment.sinapay.query_bid_info';
    }
}