<?php

namespace Ibeibeili\SinaPay\Facades;

use Illuminate\Support\Facades\Facade;

class QueryIsSetPayPassword extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'payment.sinapay.query_is_set_pay_password';
    }
}