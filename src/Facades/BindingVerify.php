<?php

namespace Ibeibeili\SinaPay\Facades;

use Illuminate\Support\Facades\Facade;

class BindingVerify extends Facade
{
  protected static function getFacadeAccessor()
  {
    return 'payment.sinapay.binding_verify';
  }
}