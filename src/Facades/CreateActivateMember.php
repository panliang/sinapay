<?php

namespace Ibeibeili\SinaPay\Facades;

use Illuminate\Support\Facades\Facade;

class CreateActivateMember extends Facade
{
  protected static function getFacadeAccessor()
  {
    return 'payment.sinapay.create_activate_member';
  }
}