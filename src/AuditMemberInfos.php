<?php

namespace Ibeibeili\SinaPay;
/**
 * 请求审核企业会员
 */
class AuditMemberInfos extends AbstractPaymentApi
{
    /**
     * 发起请求
     *
     * @param  array $params
     * @return mixed
     */
    public function send($params = [])
    {

        $params = $this->getParams($params);

        $data = $this->sinapay->createCurlData($params);

        $result = $this->sinapay->curlPost($this->config['mgs_url'], $data);

        $result = json_decode($result, true);

        return $result;
    }

    /**
     * 获取提交参数
     *
     * @param  array $params
     * @return array
     */
    protected function getParams($params = [])
    {

        $params = array_merge($this->defaultParams(), $params);

        $params = $this->pretreatment($params);

        ksort($params);

        $params['sign'] = $this->sinapay->getSignMsg($params, @$params['sign_type']);

        return $params;
    }

    public function upload($file,$filename){
        return $this->sinapay->sftpUpload($file,$filename);
    }
    /**
     * 预处理加密字段
     * 
     * @param  array  $args 加密前数据
     * @return array       加密后数据
     */
    protected function pretreatment(array $args){


        $args['license_no'] = $this->sinapay->defaultRsaEncrypt($args['license_no']);
//联系电话
        $args['telephone'] = $this->sinapay->defaultRsaEncrypt($args["telephone"]);
//联系Email
        $args['email'] = $this->sinapay->defaultRsaEncrypt($args['email']);
//组织机构代码
        $args['organization_no'] = $this->sinapay->defaultRsaEncrypt($args['organization_no']);
//企业法人
        $args['legal_person'] = $this->sinapay->defaultRsaEncrypt($args['legal_person']);
//法人证件号码
        $args['cert_no'] = $this->sinapay->defaultRsaEncrypt($args['cert_no']);
//法人手机号码
        $args['legal_person_phone'] = $this->sinapay->defaultRsaEncrypt($args['legal_person_phone']);
//银行卡号
        $args['bank_account_no'] = $this->sinapay->defaultRsaEncrypt($args['bank_account_no']);


        return $args;
    }
}