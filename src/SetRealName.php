<?php

namespace Ibeibeili\SinaPay;

class SetRealName extends AbstractPaymentApi
{
    /**
     * 发起请求
     *
     * @param  array $params
     * @return mixed
     */
    public function send($params = [])
    {
        //添加固定非空的业务参数
        $params['cert_type'] = 'IC';
        $params['identity_type'] = 'UID';

        //对隐私字段进行RSA加密
        $params['cert_no'] = $this->sinapay->defaultRsaEncrypt($params['cert_no']);
        $params['real_name'] = $this->sinapay->defaultRsaEncrypt($params['real_name']);

        $params = $this->getParams($params);

        $data = $this->sinapay->createCurlData($params);

        $result = $this->sinapay->curlPost($this->config['mgs_url'], $data);


        $result = json_decode($result, true);

        return $result;
    }

    /**
     * 获取提交参数
     *
     * @param  array $params
     * @return array
     */
    protected function getParams($params = [])
    {
        $params = array_merge($this->defaultParams(), $params);

        ksort($params);

        $params['sign'] = $this->sinapay->getSignMsg($params, @$params['sign_type']);

//        dd($params);

        return $params;
    }
}