<?php

namespace Ibeibeili\SinaPay;

/**
 *  查询新浪是否设置支付密码
 */
class SmtFundAgentBuy extends AbstractPaymentApi
{
    /**
     * 发起请求
     *
     * @param  array $params
     * @return mixed
     */
    public function send($params = [])
    {
        $params = $this->getParams($params);

        $data = $this->sinapay->createCurlData($params);

        $result = $this->sinapay->curlPost($this->config['mgs_url'], $data);

        $result = json_decode($result, true);

        return $result;
    }

    /**
     * 获取提交参数
     *
     * @param  array $params
     * @return array
     */
    protected function getParams($params = [])
    {
        $params = array_merge($this->defaultParams(), $params);
        $params['agent_name'] = $this->sinapay->defaultRsaEncrypt($params['agent_name']);
        $params['license_no'] = $this->sinapay->defaultRsaEncrypt($params['license_no']);
//联系电话
        $params['agent_mobile'] = $this->sinapay->defaultRsaEncrypt($params["agent_mobile"]);
        $params['service'] = 'smt_fund_agent_buy';

        ksort($params);

        $params['sign'] = $this->sinapay->getSignMsg($params, @$params['sign_type']);

        return $params;
    }
}