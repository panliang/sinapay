<?php

namespace Ibeibeili\SinaPay;

use Illuminate\Support\Facades\Request;

abstract class AbstractPaymentApi
{
    /**
     * 新浪支付
     *
     * @var \Ibeibeili\SinaPay\SinaPay
     */
    protected $sinapay;

    /**
     * 支付配置文件
     *
     * @var array
     */
    protected $config;

    /**
     * 接口服务 api
     *
     * @return string
     */
    protected $service;

    /**
     * 创建一个新的支付接口实例
     *
     * @param  \Ibeibeili\SinaPay\SinaPay $sinapay
     * @param  array $config
     * @return void
     */
    public function __construct($sinapay, $config)
    {
        $this->sinapay = $sinapay;
        
        $this->config = $config;
    }

    /**
     * 默认参数
     *
     * @return array
     */
    protected function defaultParams()
    {
        // 发起请求的用户 ip
        $ip = Request::ip();
        // 请求的新浪接口
        $service = $this->getService();
        // 默认参数
        $defaults = [
            '_input_charset' => @$this->config['input_charset'],
            'service' => $service,
            'version' => @$this->config['version'],
            'partner_id' => @$this->config['partner_id'],
            'request_time' => date('YmdHis'),
            'sign_type' => 'RSA',
        ];

        if(in_array($service, $this->getClientIpService())) {
            return array_merge($defaults, ['client_ip' => $ip]);
        } elseif (in_array($service, $this->getUserIpService())) {
            return array_merge($defaults, ['user_ip' => $ip]);
        } elseif (in_array($service, $this->getPayerIpService())) {
            return array_merge($defaults, ['payer_ip' => $ip]);
        }

        return $defaults;
    }

    /**
     * 获取当前调用 api 服务
     *
     * @return string
     */
    public function getService()
    {
        if (isset($this->service)) {
            return $this->service;
        }

        return snake_case(class_basename($this));
    }

    public function checkSignMsg($result){
        ksort ( $result );
        return $this->sinapay->checkSignMsg($result, $result['sign_type']);
    }

    /**
     * 获取需传递 client_ip 的服务接口
     *
     * @return array
     */
    protected function getClientIpService()
    {
        return [
            'create_activate_member',
            'set_real_name',
            'binding_verify',
            'unbinding_verify',
            'binding_bank_card',
            'binding_bank_card_advance',
            'unbinding_bank_card',
            'unbinding_bank_card_advance',
            'balance_freeze',
            'balance_unfreeze',
            'audit_member_infos',
            'smt_fund_agent_buy',
        ];
    }

    /**
     * 获取需传递 user_ip 的服务接口
     *
     * @return array
     */
    protected function getUserIpService()
    {
        return [
            'create_single_hosting_pay_trade',
            'create_batch_hosting_pay_trade',
            'create_hosting_refund',
            'create_hosting_withdraw',
            'advance_hosting_pay',
            'create_single_hosting_pay_to_card_trade',
            'create_batch_hosting_pay_to_card_trade',
            'finish_pre_auth_trade'
        ];
    }

    /**
     * 获取需传递 payer_ip 的服务接口
     *
     * @return array
     */
    protected function getPayerIpService()
    {
        return [
            'create_hosting_collect_trade',
            'pay_hosting_trade',
            'create_hosting_deposit',
            'create_hosting_transfer',
        ];
    }
}