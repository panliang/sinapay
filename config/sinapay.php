<?php

return [
    /**
     * 接口版本，新浪支付接口文档参数
     */
    'version' => '1.0',
    /**
     * 商户号，由新浪支付提供
     */   
    'partner_id' => '200004595271',
    /**
     * 商户接口字符集，新浪支付接口文档参数
     */
    'input_charset' => 'utf-8',//接口字符集编码
    /**
     * 商户签名私钥，由商户自己生成
     */
    'rsa_sign_private_key' => dirname(__File__) ."/../key/rsa_sign_private.pem",//签名私钥
    /**
     * 商户验证签名公钥，由新浪提供
     */
    'rsa_sign_public_key' => dirname ( __File__ )."/../key/rsa_sign_public.pem",//验证签名公钥
    /**
     * 新浪支付特殊参数加密，公钥，由新浪支付提供
     */
    'rsa_public_key' => dirname ( __File__ )."/../key/rsa_public.pem",
    /**
     *异步回调地址，商户自定义自己系统的回调地址
     */
    'notify_url' => 'http://139.196.46.136',
    /**
     *同步返回地址，商户自己定义自己系统的同步返回地址
     */
    'return_url'=> 'http://139.196.46.136',
    /**
     * 网关地址，接口文档参数
     */
    'mgs_url' => 'https://testgate.pay.sina.com.cn/mgs/gateway.do',//会员类网关地址
    'mas_url' => 'https://testgate.pay.sina.com.cn/mas/gateway.do',//订单类网关地址

    'debug_status' => true, //true 开启日志记录  false 关闭日志记录
    'logger_storage' => __DIR__.'/../logs', // 日志存储地址

    /**
     * sftp参数配置
     */
    //sftp地址
    'sftp_address' => '222.73.39.37',
    //sftp端口号
    'sftp_port' => '50022',
    //sftp登录名
    'sftp_username' => '200004595271',
    //sftp登录私钥
    'sftp_privatekey' => dirname ( __File__ ).'/../key/id_rsa',
    //sftp登录公钥
    'sftp_publickey' => dirname ( __File__ ).'/../id_rsa.pub',
    //sftp上传目录
    'sftp_upload_directory' => dirname ( __File__ ).'/upload'
];